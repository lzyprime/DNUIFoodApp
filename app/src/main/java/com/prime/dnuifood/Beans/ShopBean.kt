package com.prime.dnuifood.Beans

data class ShopBean(var shop_id: String,
                    var shopname: String,
                    var address: String,
                    var phonenum: String,
                    var intro: String,
                    var pic: String,
                    var comment: String,
                    var level: Float)