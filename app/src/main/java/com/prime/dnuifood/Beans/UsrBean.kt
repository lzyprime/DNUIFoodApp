package com.prime.dnuifood.Beans

data class UsrBean(
    var user_id: String,
    var username: String,
    var userpass: String,
    var mobilenum: String,
    var address: String,
    var comment: String
)